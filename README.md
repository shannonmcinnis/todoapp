The goal of this repo is a todo app that integrates with Google calendar to automatically schedule tasks.
I would like to write the scheduling algorithm in Scala, so I'm working on including Scala code in
an Android app using Bazel. I'm using Bazel because I've used it before and I'd like to get better at
it; it may not be the best tool for the job.

To build or run the app, you'll need to install Bazel: https://docs.bazel.build/versions/main/install.html.
I use Bazelisk:
```bash
sudo wget -O /usr/local/bin/bazel https://github.com/bazelbuild/bazelisk/releases/latest/download/bazelisk-linux-amd64
sudo chmod +x /usr/local/bin/bazel
```

To build:
`bazel build //src/main:app`

To run:
`bazel mobile-install //src/main:app` (you need an emulator running or a properly set-up Android device
connected)