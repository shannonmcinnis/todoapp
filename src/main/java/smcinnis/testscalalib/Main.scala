package smcinnis.lib.testscala

object Thing {
  def useless(): Int = {
    3
  }
}

object Main extends App {
  println(Thing.useless())
}