package smcinnis.mainapp;
import smcinnis.lib.testscala.Thing;

/**
 * A tiny Greeter library for the Bazel Android "Hello, World" app.
 */
public class Greeter {
  public String sayHello() {
    return Integer.toString(Thing.useless()) + "Hello Bazel! \uD83D\uDC4B\uD83C\uDF31"; // Unicode for 👋🌱
  }
}
